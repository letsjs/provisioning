#!/usr/bin/env bash

MYSQLPASSWORD=admin

sudo apt-get -y update

sudo apt-get -y install apache2 php5 libapache2-mod-php5 php5-mcrypt php5-curl php5-mysql php5-gd php5-cli php5-dev mysql-client
php5enmod mcrypt git

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password $MYSQLPASSWORD'

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password $MYSQLPASSWORD'

sudo apt-get -y install mysql-server

sudo service apache2 restart && sudo service mysql restart




sudo php5enmod mcrypt

sudo a2enmod rewrite


curl -sS https://getcomposer.org/installer | php

sudo mv composer.phar /usr/local/bin/composer


cd ~/dev/letjs

sudo composer install

PATH=$PATH:~/.composer/vendor/bin/

sudo cp -f ~/dev/provisioning/apache2.conf /etc/apache2/

sudo cp -f ~/dev/provisioning/letsjs.conf /etc/apache2/sites-available/

sudo a2ensite letsjs.conf

sudo service apache2 restart

mysql -uroot -p$MYSQLPASSWORD -e "CREATE DATABASE letsjs;"

