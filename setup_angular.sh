#!/usr/bin/env bash

 sudo cp -f ~/dev/provisioning/apache2.conf /etc/apache2/

 sudo cp -f ~/dev/provisioning/angular.conf /etc/apache2/sites-available/

 sudo a2ensite angular.conf

 sudo service apache2 restart

 